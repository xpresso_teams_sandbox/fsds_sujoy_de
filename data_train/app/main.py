"""
This is the implementation of data preparation for sklearn
"""

import sys
import pandas as pd
import lightgbm as lgb
import pickle
import os
import numpy as np
import pandas as pd
import logging

from sklearn.metrics import accuracy_score, roc_auc_score
from sklearn.model_selection import train_test_split

from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

logger = XprLogger("data_train",level=logging.INFO)

features_to_use = ['cns_score_bucket', 'branch_id', 'percent_pri_default', \
                   'DELINQUENT.ACCTS.IN.LAST.SIX.MONTHS', 'NO.OF_INQUIRIES','ltv', \
                   'ratio_sec_default_active', 'AVERAGE.ACCT.AGE', 'NEW.ACCTS.IN.LAST.SIX.MONTHS']

BASE_PATH = '/data'

class DataTrain(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, train_file_path):
        super().__init__(name="DataTrain")
        """ Initialize all the required constansts and data her """
        self.train_data = pd.read_csv(train_file_path)


    @staticmethod
    def calculate_accuracy(y_actual, y_pred):
        return accuracy_score(y_actual, y_pred)

    @staticmethod
    def calculate_roc(y_actual, y_pred):
        return roc_auc_score(y_actual, y_pred)

    @staticmethod
    def run_lgb(train_X, train_y, val_X, val_y):
        params = {
            "objective": "binary",
            "metric": ['error', 'auc'],
            "num_leaves": 60,
            "learning_rate": 0.05,
            "max_depth": 8,
            "bagging_fraction": 0.65,
            "feature_fraction": 0.65,
            "bagging_frequency": 5,
            "bagging_seed": 2019,
            "verbosity": -1
        }

        lgtrain = lgb.Dataset(train_X, label=train_y)
        lgval = lgb.Dataset(val_X, label=val_y)
        evals_result = {}
        model = lgb.train(params, lgtrain, 5000, valid_sets=[lgtrain, lgval],\
                          early_stopping_rounds=100, verbose_eval=50, \
                          evals_result=evals_result)
        return model


    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===

            self.features_to_use = features_to_use
            X_train, X_test, y_train, y_test = train_test_split(self.train_data[features_to_use], \
                                                                self.train_data['loan_default'], \
                                                                test_size=0.2, \
                                                                random_state=12)
            logging.info("Training lightgbm started")
            model = self.run_lgb(X_train, y_train, X_test, y_test)
            logging.info("Training lightgbm ended")
            train_preds_prob = model.predict(X_train)
            val_preds_prob = model.predict(X_test)
            train_predictions = [int(x >= 0.5) for x in train_preds_prob]
            val_predictions = [int(x >= 0.5) for x in val_preds_prob]
            train_acc_score = self.calculate_accuracy(y_train, train_predictions)
            train_roc_score = self.calculate_roc(y_train, train_preds_prob)
            test_acc_score = self.calculate_accuracy(y_test, val_predictions)
            test_roc_score = self.calculate_roc(y_test, val_preds_prob)
            metrics = {
                'Training accuracy': train_acc_score,
                'Training ROC AUC': train_roc_score,
                'Validation accuracy': test_acc_score,
                'Validation ROC AUC': test_roc_score
            }
            self.send_metrics(f'Training and validation metric : {metrics}')
            logging.info("Saving model")
            pickle.dump(model, open(f"{BASE_PATH}/lgb_model.pkl", 'wb'))
            logging.info("Saving model")
            self.completed(model, push_exp=True)
        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed()

    def send_metrics(self, value):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "lightgbm Metrics"},
                "metric": {"metric_key": value}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = DataTrain(train_file_path=os.path.join(BASE_PATH, 'modified_train.csv'))
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
