"""
This is the implementation of data preparation for sklearn
"""

import sys
import os
import pandas as pd
import numpy as np
import logging
import re
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

logger = XprLogger("data_prep",level=logging.INFO)

BASE_PATH = '/data'


class DataPrep(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, train_file_path):
        super().__init__(name="DataPrep")
        """ Initialize all the required constansts and data her """
        self.train_data = pd.read_csv(train_file_path)

    @staticmethod
    def get_months(x):
        nos = [int(y) for y in re.findall(r'\d+', x)]
        months = nos[0] * 12 + nos[1]
        return months

    @staticmethod
    def bucketize_score(x):
        score_bucket = 0
        x = x.lower()
        if 'very low' in x:
            score_bucket = 1
        elif 'low' in x:
            score_bucket = 2
        elif 'medium' in x:
            score_bucket = 3
        elif 'very high' in x:
            score_bucket = 5
        elif 'high' in x:
            score_bucket = 4
        return score_bucket

    @staticmethod
    def get_pri_ratio(x):
        try:
            ratio = x['PRI.OVERDUE.ACCTS'] / x['PRI.NO.OF.ACCTS']
        except:
            ratio = 0
        return ratio

    @staticmethod
    def get_sec_ratio(x):
        try:
            ratio = x['SEC.OVERDUE.ACCTS'] / x['SEC.ACTIVE.ACCTS']
        except:
            ratio = 0
        return ratio

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """

        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===
            logging.info("Preparing data")
            self.train_data['AVERAGE.ACCT.AGE'] = self.train_data['AVERAGE.ACCT.AGE'].apply(lambda x: self.get_months(x)).astype(int)
            self.train_data['CREDIT.HISTORY.LENGTH'] = self.train_data['CREDIT.HISTORY.LENGTH'].apply(lambda x: self.get_months(x)).astype(int)
            logging.info("Converted AVERAGE.ACCT.AGE and CREDIT.HISTORY.LENGTH to int")
            self.train_data.loc[self.train_data['Employment.Type'] == '', 'Employment.Type'] = 'unknown'
            logging.info("Filled missing values of employment type")
            dat_cols = ['Date.of.Birth', 'DisbursalDate']
            for col in dat_cols:
                self.train_data[col] = pd.to_datetime(self.train_data[col], format="%d-%m-%y")
            self.train_data['birth_year'] = self.train_data['Date.of.Birth'].dt.year
            self.train_data['disbursal_year'] = self.train_data['DisbursalDate'].dt.year
            logging.info("Prepared birth year and disbursal year columns")
            self.train_data['age_at_loan'] = self.train_data['disbursal_year'] - self.train_data['birth_year']
            logging.info("Prepared age at loan")
            self.train_data.loc[self.train_data['age_at_loan'] < 0, 'birth_year'] = self.train_data.loc[self.train_data['age_at_loan'] < 0, 'birth_year'] - 100
            self.train_data['age_at_loan'] = self.train_data['disbursal_year'] - self.train_data['birth_year']
            logging.info("Corrected erroneous birth year data")
            self.train_data['cns_score_bucket'] = self.train_data['PERFORM_CNS.SCORE.DESCRIPTION'].apply(lambda x: self.bucketize_score(x))
            self.train_data['percent_pri_default'] = self.train_data.apply(lambda x: self.get_pri_ratio(x), axis=1)
            self.train_data['ratio_sec_default_active'] = self.train_data.apply(lambda x: self.get_sec_ratio(x), axis=1)
            self.send_metrics(f'Prepared train data : {self.train_data.shape}')
            self.completed(push_exp=True)
        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed()

    def send_metrics(self, value):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": value}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        self.train_data.to_csv(os.path.join(BASE_PATH, 'modified_train.csv'), index=False)
        logging.info("Data saved")
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py
    training_data_file = 'train.csv'
    data_prep = DataPrep(train_file_path=os.path.join(BASE_PATH, training_data_file))
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
